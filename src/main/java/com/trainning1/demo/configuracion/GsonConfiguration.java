package com.trainning1.demo.configuracion;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.trainning1.demo.user.domain.Hora;
import com.trainning1.demo.user.domain.Minuto;
import com.trainning1.demo.user.domain.Segundo;
import com.trainning1.demo.user.domain.UnidadTiempo;
import com.trainning1.demo.user.serialize.HoraSearialize;
import com.trainning1.demo.user.serialize.MinutoSearialize;
import com.trainning1.demo.user.serialize.SegundoSearialize;
import com.trainning1.demo.user.serialize.UnidadSearialize;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GsonConfiguration {

    @Bean
    public Gson gson(){
        return new GsonBuilder()
                .registerTypeAdapter(Hora.class, new HoraSearialize())
                .registerTypeAdapter(Minuto.class, new MinutoSearialize())
                .registerTypeAdapter(UnidadTiempo.class, new UnidadSearialize())
                .registerTypeAdapter(Segundo.class, new SegundoSearialize())
                .create();
    }
}
