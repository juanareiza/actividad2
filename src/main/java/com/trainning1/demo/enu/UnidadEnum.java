package com.trainning1.demo.enu;

import com.trainning1.demo.user.domain.Hora;
import com.trainning1.demo.user.domain.Minuto;
import com.trainning1.demo.user.domain.Segundo;

import java.time.LocalTime;

public enum UnidadEnum {
    MILISECONDS,
    SECONDS,
    MINUTES,
    HOURS;

    public static Long converter(UnidadEnum unidad, LocalTime localTime){
        Hora hora = Hora.of(localTime.getHour());
        Minuto minuto = Minuto.of(localTime.getMinute());
        Segundo segundo = Segundo.of(localTime.getSecond());
        int convertHour;
        int converMinutes;
        int convertSecond;
        int resulta;
        switch (unidad){
            case HOURS:
                converMinutes = minuto.getMinuto()/60;
                convertSecond = segundo.getSegundo()/3600;
                resulta =  hora.getHora() + converMinutes + convertSecond;
                return (long) resulta;
            case MINUTES:
                convertHour = hora.getHora()*60;
                convertSecond = segundo.getSegundo()/60;
                resulta = convertHour+ minuto.getMinuto() + convertSecond;
                return (long) resulta;
            case SECONDS:
                convertHour = hora.getHora()*3600;
                converMinutes = minuto.getMinuto()/60;
                resulta = convertHour+ segundo.getSegundo() + converMinutes;
                return (long) resulta;
            case MILISECONDS:
                convertHour = hora.getHora()/3600000;
                converMinutes = minuto.getMinuto()*60000;
                convertSecond = segundo.getSegundo()*1000;
                resulta = converMinutes + convertHour + convertSecond;
                return (long) resulta;
                default:
                    throw  new UnsupportedOperationException();
        }
    }
}
