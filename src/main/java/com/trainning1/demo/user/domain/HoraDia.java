package com.trainning1.demo.user.domain;

import com.trainning1.demo.enu.UnidadEnum;
import com.trainning1.demo.user.precondiciones.Precondiciones;
import lombok.Value;

import java.time.LocalTime;


public class HoraDia {
    Integer hora;
    Integer minutos;
    Integer segundo;
    Long cantidadHoras;

    public HoraDia(UnidadEnum unidadEnum, LocalTime localTime){
        cantidadHoras = UnidadEnum.converter(unidadEnum, localTime);
        this.hora= localTime.getHour();
        this.minutos = localTime.getMinute();
        this.segundo = localTime.getSecond();
    }
}
