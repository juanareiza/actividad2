package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import lombok.Value;

@Value(staticConstructor = "of")
public class Minuto {
    Integer minuto;

    private Minuto(Integer minuto){
        Precondiciones.conditionNoNul(minuto);
        Precondiciones.conditionMinuto(minuto);
        this.minuto = minuto;
    }
}
