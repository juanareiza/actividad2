package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import lombok.Value;

@Value(staticConstructor = "of")
public class Hora {
    Integer hora;

    private Hora(Integer hora){
        Precondiciones.conditionNoNul(hora);
        Precondiciones.conditionHora(hora);
        this.hora= hora;
    }
}
