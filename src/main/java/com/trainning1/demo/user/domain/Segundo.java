package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import lombok.Value;

@Value(staticConstructor = "of")
public class Segundo {
    Integer segundo;

    private Segundo(Integer segundo){
        Precondiciones.conditionNoNul(segundo);
        Precondiciones.conditionSegundo(segundo);
        this.segundo = segundo;
    }
}
