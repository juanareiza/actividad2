package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import lombok.Data;
import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("demo")
 public class UnidadTiempo {

   String unidadTiempo;

    public String getUnidadTiempo() {
        return unidadTiempo;
    }

    @Override
    public String toString() {
        return "UnidadTiempo{" +
                "unidadTiempo='" + unidadTiempo + '\'' +
                '}';
    }

    public void setUnidadTiempo(String unidadTiempo) {
        Precondiciones.conditionUnidadTiempo(unidadTiempo);
        this.unidadTiempo = unidadTiempo;
    }
}
