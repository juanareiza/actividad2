package com.trainning1.demo.user.serialize;

import com.google.gson.*;
import com.trainning1.demo.user.domain.Hora;
import com.trainning1.demo.user.domain.Minuto;

import java.lang.reflect.Type;

public class MinutoSearialize implements JsonSerializer<Minuto>, JsonDeserializer<Minuto> {
    @Override
    public Minuto deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return null;
    }

    @Override
    public JsonElement serialize(Minuto minuto, Type type, JsonSerializationContext jsonSerializationContext) {
       Integer value =  minuto.getMinuto();
        return new JsonPrimitive(value);
    }
}
