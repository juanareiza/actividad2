package com.trainning1.demo.user.serialize;

import com.google.gson.*;
import com.trainning1.demo.user.domain.Segundo;
import com.trainning1.demo.user.domain.UnidadTiempo;

import java.lang.reflect.Type;

public class UnidadSearialize implements JsonSerializer<UnidadTiempo>, JsonDeserializer<UnidadTiempo> {
    @Override
    public UnidadTiempo deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return null;
    }

    @Override
    public JsonElement serialize(UnidadTiempo unidadTiempo, Type type, JsonSerializationContext jsonSerializationContext) {
       String value =  unidadTiempo.getUnidadTiempo();
        return new JsonPrimitive(value);
    }
}
