package com.trainning1.demo.user.serialize;

import com.google.gson.*;
import com.trainning1.demo.user.domain.Minuto;
import com.trainning1.demo.user.domain.Segundo;

import java.lang.reflect.Type;

public class SegundoSearialize implements JsonSerializer<Segundo>, JsonDeserializer<Segundo> {
    @Override
    public Segundo deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return null;
    }

    @Override
    public JsonElement serialize(Segundo segundo, Type type, JsonSerializationContext jsonSerializationContext) {
       Integer value =  segundo.getSegundo();
        return new JsonPrimitive(value);
    }
}
