package com.trainning1.demo.user.serialize;

import com.google.gson.*;
import com.trainning1.demo.user.domain.Hora;

import java.lang.reflect.Type;

public class HoraSearialize implements JsonSerializer<Hora>, JsonDeserializer<Hora> {
    @Override
    public Hora deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return null;
    }

    @Override
    public JsonElement serialize(Hora hora, Type type, JsonSerializationContext jsonSerializationContext) {
       Integer value =  hora.getHora();
        return new JsonPrimitive(value);
    }
}
