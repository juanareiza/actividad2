package com.trainning1.demo.user.controller;

import com.trainning1.demo.enu.UnidadEnum;
import com.trainning1.demo.user.domain.HoraDia;
import com.trainning1.demo.user.domain.UnidadTiempo;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;

@RestController
@AllArgsConstructor
@RequestMapping("/trainning/v1/")
public class UserController {
    UnidadTiempo unidadTiempo;
    @GetMapping("horadia")
    public HoraDia horaDia(){
        LocalTime calendar = LocalTime.now();
        HoraDia horaDia = new HoraDia(UnidadEnum.valueOf(unidadTiempo.getUnidadTiempo()), calendar);
        return horaDia; 
    }
}
