package com.trainning1.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;

@SpringBootApplication(exclude = {JacksonAutoConfiguration.class})
public class Trainning1Application {

	public static void main(String[] args) {
		SpringApplication.run(Trainning1Application.class, args);
	}

}
